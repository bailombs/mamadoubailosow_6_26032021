//import { data } from "./data.js";
const displayPhotographers = (photographers) => {
  const path = "css/images/FishEys-Photos/Photographers-ID-Photos/";
  let elemMain= document.querySelector('.main');
  let mainTitle = document.createElement('h1');
  elemMain.appendChild(mainTitle);
  mainTitle.classList.add("main__title");
  mainTitle.innerHTML = "Nos Photographes";

  
  
    for (let photographer of photographers) {    
     
            // div Parent
      let newLink = document.createElement('div');
      newLink.classList.add("main__bloc");
      newLink.setAttribute("role","complementary");
      elemMain.appendChild(newLink);

      let photogLinkPage= document.createElement('a');
      photogLinkPage.href="#";
      newLink.appendChild(photogLinkPage);
  
      let newDiv3 = document.createElement('aside');
      newDiv3.classList.add("main__bloc--photo");
     
     
      photogLinkPage.appendChild(newDiv3);
      
      //photogLinkPage.href="photographe"+i+".html";
      photogLinkPage.href="photographe.html?id="+ photographer.id;
      photogLinkPage.setAttribute("arial-current", "page");

      // image img enfant de  div
      let newPhoto = document.createElement('img');    
    
      newPhoto.src = path + photographer.portrait;
      newPhoto.alt = photographer.name;
      // newPhoto.classList.add("main__bloc--photo")
      newDiv3.appendChild(newPhoto); 
      // titre enfant de de div
      let newTitle = document.createElement('h2');  
      photogLinkPage.appendChild(newTitle);  
      newTitle.innerHTML = `${photographer.name}`; 
      // paragraphe enfant de div
      let newParagraph = document.createElement('p');
      newLink.appendChild(newParagraph);
      newParagraph.innerHTML=`<span class="address">${photographer.city},</span>
                              <span class="address">${photographer.country}</span><br>
                              <span class="tag">${photographer.tagline}</span><br>
                              <span class="price">${photographer.price}€/jour</span>`;

      let divTag= document.createElement('aside');
      divTag.classList.add('main__bloc--blocTag');
      newLink.appendChild(divTag);
    for( let tag of photographer.tags) {
      let linkTag = document.createElement('a');
      let spanTag = document.createElement('span');
      spanTag.setAttribute("role","complementary");
      divTag.appendChild(linkTag);
      linkTag.appendChild(spanTag);
      spanTag.innerHTML= '#'+tag+' ';
        
    }
  }
 
}
// fonction affichage des photographes selon le tag
export const displayPhotograhersByTag = (data, eltTags) => {
 
 
  if (eltTags.length === 0) {
    displayPhotographers(data.photographers)  
  } else {
    //convertir tous les elements du tableau  de navigation en minuscules 
    const lowerCaseElttags =  eltTags.map(elt => { return elt.toLowerCase()});

    const byTags = data.photographers.filter((photographer) => { 
       
      return photographer.tags.some(tag => { return lowerCaseElttags.includes(tag)})
      
    })

    displayPhotographers(byTags);                
  }
}


















  
