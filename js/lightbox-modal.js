import { MediaFactory } from "./media_factory.js";

let currentMedias = []; // tableau d'images
let currentMediaIndex = -1; // index du tableau

//  fonction Lightbox
export const lightBox_Modal = (photographerId, textContent, mediaId) => {
    const modalContentBlocImage = document.querySelector('.modal__content--blocimage') ;
   
    const modalContentText = document.querySelector('.modal__content--blocimage > figcaption'); 
   

    
    //recuperation de l'indice du tableau CurrentMedia 
    currentMediaIndex = currentMedias.findIndex((media)=> media.id === mediaId); 
   
    modalContentBlocImage.innerHTML  = "";
            
    const mediaFactory = new MediaFactory(currentMedias[currentMediaIndex], photographerId,currentMedias[currentMediaIndex].alt);
    const media = mediaFactory.createElement(currentMedias[currentMediaIndex].title);           
    modalContentBlocImage.appendChild(media);
         
    modalContentBlocImage.appendChild(modalContentText);
    modalContentText.innerHTML = textContent;  

}

// fonction  image suivante
export const next = (photographerId) =>{
    const modalContentBlocImage = document.querySelector('.modal__content--blocimage');
    const modalContentText = document.querySelector(' .modal__content--blocimage > figcaption'); 
    currentMediaIndex +=1; 
     
    if(currentMediaIndex === currentMedias.length){
        currentMediaIndex = 0;
    }

    modalContentBlocImage.innerHTML = "";       
    let nextImage = currentMedias[currentMediaIndex];

  
    const mediaFactory = new MediaFactory(currentMedias[currentMediaIndex], photographerId);
    const media = mediaFactory.createElement(currentMedias[currentMediaIndex].title);        
        
                            
    modalContentBlocImage.appendChild(media);
    modalContentBlocImage.appendChild(modalContentText);
    modalContentText.innerHTML= nextImage.title;
}

// fonction image precedente
export const previous = (photographerId) =>{
    const modalContentBlocImage = document.querySelector('.modal__content--blocimage');   
    const modalContentText = document.querySelector('.modal__content--blocimage > figcaption'); 
    if(currentMediaIndex === 0 ) {
        currentMediaIndex = currentMedias.length;
    }   

    modalContentBlocImage.innerHTML = "";
    currentMediaIndex -=1;      
    let nextImage = currentMedias[currentMediaIndex];
  
  
    const mediaFactory = new MediaFactory(currentMedias[currentMediaIndex], photographerId);
    const media = mediaFactory.createElement(currentMedias[currentMediaIndex].title);        
        
                            
    modalContentBlocImage.appendChild(media);   
    modalContentBlocImage.appendChild(modalContentText) ;  
    modalContentText.innerHTML = nextImage.title;
}


export const launchModal = (medias) => {
    currentMedias = medias;
    const modalDialog = document.querySelector('.modal');
    modalDialog.style.display = "block";
}

export const closeLightBoxModal = () => {
    const modalDialog = document.querySelector('.modal');
    modalDialog.style.display = "none";
}

//fonction accessibilty

const mainWrapper= document.querySelector('#main-wrapper');
const modal = document.querySelector('.modal');
const modalCloseButton = document.querySelector('.modal__content--close');


export const onOpenModal = () => {
    mainWrapper.setAttribute('aria-hidden','true');
    modal.setAttribute('aria-hidden', 'false');
    modalCloseButton.focus();
 
}
export const onCloseModal = (modalOpenButton) => {
    mainWrapper.setAttribute('aria-hidden','false');
    modal.setAttribute('aria-hidden', 'true'); 
    modalOpenButton.focus();      
}










