export class MediaFactory {
    constructor(media, photographerId){
        
      if(media.image){
        return new ImageFactory(media.image, photographerId);
      } else {
           return new VideoFactory(media.video, photographerId)
      } 
   
    }    
}

class ImageFactory{
    constructor(source, photographerId){
        this.source = "css/images/FishEys-Photos/"+ photographerId+"/"+ source;       
    }
    createElement(text){
        const image= document.createElement('img');
        image.src= this.source;
        image.alt = text;
        return image;
    }
}

class VideoFactory {
    constructor(source, photographerId){
        this.source = "css/images/FishEys-Photos/"+ photographerId+"/"+ source;
    }
    createElement() {
        const video = document.createElement('video');
        video.src = this.source;
        video.controls = "controls";
        return video;
    }
}