

//import { data } from "./data.js";
import { formModal, onCloseModalForm, onOpenModalForm } from "./form-modal.js";
import {closeLightBoxModal, launchModal, lightBox_Modal, next, onCloseModal, onOpenModal, previous } from "./lightbox-modal.js";


const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);
const id = urlParams.get('id');
const numberId= parseInt(id,10);


let existingLikesCounter = 0;
let totalLikesCounter = 0;


const displayPhotographer = async () => {

  const dataFile = await fetch("./js/data.json");
  const data = await dataFile.json();
 

  let headerPhotog = document.querySelector('.header-photog');
  let headerPhotogLogo = document.createElement('div');
  headerPhotogLogo.setAttribute("aria-labelledby", "homelogo");
  let photogLinkLogo = document.createElement('a');
  let imageLogo = document.createElement('img');
  imageLogo.id ="homelogo";
  let descriptPhotog = document.createElement('div');
  descriptPhotog.classList.add('header-photog__bloc');
  descriptPhotog.setAttribute("role","complementary");

  headerPhotog.appendChild(headerPhotogLogo);
  headerPhotogLogo.appendChild(photogLinkLogo);
  headerPhotogLogo.classList.add('header-photog__logoBloc');
  headerPhotog.appendChild(descriptPhotog);
  photogLinkLogo.appendChild(imageLogo);
  photogLinkLogo.href = "index.html";
  imageLogo.src="css/images/Logo/Logo.png";
  imageLogo.alt="Fisheye Home page - page d'accueil";

  //Appels des fonctions description photographes et realiastion
  descriptionPhotographer(descriptPhotog, data);
  realisationPhotographer(data);
}




// fonctions  Description photographes
const descriptionPhotographer = (blocDescript, data) => {


  const photographer = data.photographers.find(
    (p) => { 
      return p.id === numberId;
    }
  );
  

 
  let photogName = document.createElement('h1');
  photogName.classList.add('header-photog__bloc--title');

  blocDescript.appendChild(photogName);
  photogName.innerHTML = `${photographer.name}`; 
 
  let newPhotogAddress = document.createElement('p');
  newPhotogAddress.classList.add('header-photog__bloc--text');
  blocDescript.appendChild(newPhotogAddress);
  newPhotogAddress.innerHTML = `<span class="address" role="complementary" aria-label="photographer-city">${photographer.city},</span>
  <span class="address" role="complementary" aria-label"photographer-country">${photographer.country}</span><br>
  <span class="tag" role="complementary" aria-label="photographer-tagline">${photographer.tagline}</span><br>`;

  // Bouton contact photographe  
  const photogContact = document.createElement('div');
  photogContact.setAttribute("role","button");
  photogContact.classList.add('header-photog__bloc--button');
  
  photogContact.setAttribute("aria-label", "bouton contactez-moi");
  blocDescript.appendChild(photogContact);
  photogContact.innerHTML = "Contactez-moi";

  let  photogTags= document.createElement('div');
  photogTags.classList.add('header-photog__bloc--blocTag');
  blocDescript.appendChild(photogTags);

  for( let tag of photographer.tags) {
    let linkTag = document.createElement('a');
    let spanTag = document.createElement('span');
    spanTag.setAttribute("role", "complementatry");
    photogTags.appendChild(linkTag);
    linkTag.appendChild(spanTag);
    spanTag.innerHTML= '#'+tag+'';

  }

  const path = "css/images/FishEys-Photos/Photographers-ID-Photos/";
  let photogImage = document.createElement('div');
  photogImage.classList.add('header-photog__bloc--photo');
  blocDescript.appendChild(photogImage);
  let photogPhoto = document.createElement('img');
  photogImage.appendChild(photogPhoto);
  photogPhoto.src= path + photographer.portrait;
  photogPhoto.alt= photographer.name;

  const bground = document.createElement('div');
  bground.setAttribute("role","dialog");  
  bground.classList.add('bground');
  blocDescript.appendChild(bground) ;
  const bgroundContentClose = document.querySelector('.bground__content--close');
  
   // Appel fonction Modal
  formModal(bground, photographer.name, bgroundContentClose);

  // Evenement Listener 
  photogContact.addEventListener('click', ()=>{ 
    launchFormModal()
    onOpenModalForm(bground) //appel fonction accesibility
  });

  bgroundContentClose.addEventListener('click',()=>{
    closeModalForm(bground)
    onCloseModalForm(bground, photogContact) // appel fonction accessibilty 
  });
    
}



/* fonction realisation photographe */
const realisationPhotographer = (data) => {
 
  const medias = data.media.filter(
    (media) => { 
     return media.photographerId === numberId ;
    
  });
   existingLikesCounter = medias.reduce((total, media) =>{
    return total + media.likes
   }, 0)
  
  //DOM
  const realisationPhotog = document.querySelector('.realisation');
  const realisationMenu = document.createElement('div');
  realisationMenu.setAttribute("role","complementary");
  const blockSelector =  document.createElement('div')
  blockSelector.setAttribute("role", "region");
  //blockSelector.setAttribute("aria-label", "menu-deroulant");
  blockSelector.setAttribute("labelledby","menu");
  const selectfield = document. createElement('div');
  const selectFieldList = document.createElement('ul'); 
  let realisationPhotogTitle = document.createElement('span');
  realisationPhotogTitle.setAttribute("role","complementary");
  let arrow = document.createElement('span');
  
        
  realisationPhotog.appendChild(realisationMenu);
  realisationMenu.appendChild(realisationPhotogTitle); 
  realisationMenu.appendChild(blockSelector);
  blockSelector.appendChild(selectfield);
  blockSelector.appendChild(selectFieldList);
  blockSelector.appendChild(arrow);
  

  realisationMenu.classList.add('realisation__menu')
  blockSelector.classList.add('selector');  
  selectfield.setAttribute("aria-label"," menu-deroulant");
  selectfield.setAttribute("aria-labelledBy","menubar")
  selectfield.classList.add('selector__selectfield');
  selectFieldList.classList.add('selector__selectfieldlist')
  arrow.classList.add('selector__selectfield--arrow');  
  realisationPhotogTitle.classList.add('realisation__menu--parag');
  realisationPhotogTitle.innerHTML = "Trier Par";
 
  selectFieldList.name = "tri";
  selectFieldList.classList.add('hide');
 
  selectFieldList.id="menubar";
  arrow.innerHTML = '<i aria-hidden = "true" class="fas fa-chevron-down"></i>';
  const chevronUp = arrow.querySelector('i');
 


 selectfield.addEventListener('click', ()=> {
  chevronUp.classList.toggle('rotate'); 
  selectFieldList.classList.toggle('hide');
    
   if(selectFieldList.classList.contains('hide')){    
      closeDropdown(selectfield);
   } else {
      openDropdown(selectfield);  
   }      
 });
  
  selectfield.innerHTML =" Popularité";
 
  selectFieldList.innerHTML = `<li class="selector__selectfieldlist--option" role="menuitem">Popularit&eacute;</li>
                               <li class="selector__selectfieldlist--option" role="menuitem">Date</li>
                               <li class="selector__selectfieldlist--option" role="menuitem">Titre</li>`;
  
 
 
 
  const lists = document.querySelectorAll('.selector__selectfieldlist--option');
                          
  for (let list of lists) {
    
    list.addEventListener('click',  () => {     
      const listText = list.textContent;
      selectfield.innerHTML = listText;
     chevronUp.classList.toggle('rotate'); 
      selectFieldList.classList.toggle('hide')
      
        
       if(listText === "Popularité") {  
        realisContentPhoto.innerHTML = "";   
        const mediasPopularity = byPopularity(medias);
        displayMedias(mediasPopularity, realisContentPhoto,data);
        
      } 
      else if(listText === "Date") { 
        realisContentPhoto.innerHTML ="";     
        const mediasDate = byDate(medias);
        displayMedias(mediasDate, realisContentPhoto,data);       
      } 
       else if(listText === "Titre") {
        realisContentPhoto.innerHTML ="";
        const mediasTitle = byTitle(medias);
        displayMedias(mediasTitle, realisContentPhoto, data);                             
      } 

    });  
  }
             
  
  const realisContentPhoto = document.createElement('section');
  realisContentPhoto.classList.add('realisation__contentPhoto');
  
  realisationPhotog.appendChild(realisContentPhoto);
      
  displayMedias(medias, realisContentPhoto, data); // appel de l'affichage des medias
    
}


const modalContentRightIcon = document.querySelector('.modal__content--righticon');
modalContentRightIcon.addEventListener('click', () =>{
        next(numberId)
});

const modalContentLeftIcon = document.querySelector('.modal__content--lefticon');
modalContentLeftIcon.addEventListener('click', () =>{
        previous(numberId)
});


// fonctions 

//modal formulaire 
const launchFormModal = () =>{
  const bground = document.querySelector('.bground');
  bground.style.display = "block";
}

// fermeture modal
const closeModalForm = () =>{
  const bground = document.querySelector('.bground');
  bground.style.display = "none";
}

// fonction tri par popularité
const byPopularity = (medias) => { return medias.sort(
  (popularity1, popularity2) => {
    return popularity1.likes - popularity2.likes
})}

// fonction tri par  Date
const byDate = (medias) => { return medias.sort(
  (date1, date2) => {
  let firstDate = new Date(date1.date).toISOString().split('T')[0];
  let secondDate = new Date(date2.date).toISOString().split('T')[0];
  if(firstDate > secondDate){    
    return 1;
  }
   if(firstDate < secondDate){
    return -1;
  }
   return 0;  
    
})}

// fonction tri par Titre
const byTitle = (medias) => { return medias.sort(
  (title1,  title2) => {     
    if (title1.title.toLowerCase() > title2.title.toLowerCase()) {
      return 1
    }
    if (title1.title.toLowerCase() < title2.title.toLowerCase()) {
      return -1
    }
    
    return 0
})}




// Appel de la fonction displayPhotograhers 
 const  displayMedias = (medias, realisationContentPhotos, data)=> {
  const photographer = data.photographers.find(
    (p) => { 
      return p.id === numberId;
    }
  );

  for(let media of medias) {
    const imagePath = "css/images/FishEys-Photos/"+ media.photographerId+"/"+media.image;
    const videoPath = "css/images/FishEys-Photos/"+ media.photographerId+"/"+media.video;   
    const globalContentPhoto = document.createElement('div');
    let contentPhotoBloc = document.createElement('aside');
    contentPhotoBloc.classList.add('realisation__contentPhoto--bloc');
    

    if(media.image) {
      let photosPhotographer = document.createElement('img');
      photosPhotographer.src = imagePath; 
      photosPhotographer.alt = media.title;   
      contentPhotoBloc.appendChild(photosPhotographer);
    } else {
      let videosPhotographer = document.createElement('video');
      let source = document.createElement("source");
      videosPhotographer.appendChild(source)
      source.src = videoPath;
      source.type ="video/mp4";        
      videosPhotographer.poster = "";              
      contentPhotoBloc.appendChild(videosPhotographer);
      
    }
    

    realisationContentPhotos.appendChild(globalContentPhoto);
    
    globalContentPhoto.appendChild(contentPhotoBloc);
   
     // AddEvenement listener 
    contentPhotoBloc.addEventListener('click', ()=> {
     launchModal(medias)
     lightBox_Modal(media.photographerId, media.title, media.id)
     onOpenModal() //appel fontion accessibilé ouverture modal
    });
              

    let belowTextPhoto = document.createElement('div');    
    globalContentPhoto.appendChild(belowTextPhoto);   
    belowTextPhoto.classList.add('realisation__contentPhoto--text');
    belowTextPhoto.innerHTML = `<span class="text"> ${media.title}</span>                                
         <span class="likes">${media.likes}</span>
         <button type="button" aria-label="icon-like"  class="fas fa-heart"></button>
         <span class ="screen-reader"> Icon pour liker</span>`;
    
                         
    const closeModal = document.querySelector('.modal__content--close');
     //  Evenenemt listener pour fermer  la modale 
    closeModal.addEventListener('click', () => {
      closeLightBoxModal()
      onCloseModal(contentPhotoBloc) //appel fonction accesibility fermetur modal
    });
   
   const iconLikes = belowTextPhoto.querySelectorAll('button');
   const currentLikes = belowTextPhoto.querySelector('.likes');
   let like = 0;
 

   const totalLikes = document.querySelector('.total');     
    
    totalLikes.innerHTML = `<span class="total__likes">${existingLikesCounter + totalLikesCounter}</span>
    <i class="fas fa-heart"></i>
    <span class="total__price">${photographer.price}€ / Jour</span>`;
     // Evenement listener pour liker
    iconLikes.forEach(elt => elt.addEventListener('click', ()=>{
     
      like++; 
      totalLikesCounter = totalLikesCounter + 1;      
      currentLikes.textContent = media.likes + like;   
      const totalLikes = document.querySelector('.total__likes');
      totalLikes.innerHTML = `${existingLikesCounter + totalLikesCounter}`;

    })) ;
                 
  }
  
}

displayPhotographer();

//Accessibility clavier: fermeture du formulaire, de la modale  avec Echap et suivant, precedent
 const checkKeypress = (e) => {
  const keyCode = e.keyCode ? e.keyCode : e.which;    
  if( keyCode === 27){
   closeModalForm()
  }
  if( keyCode === 27){
    closeLightBoxModal()
  }
  if( keyCode === 37){
    previous(numberId);
  }
  if( keyCode === 39){      
    next(numberId);
  }  
}

document.addEventListener("keydown", checkKeypress, false );

// fonction accessibility menu deroulant
const closeDropdown = (eltDom) => {
  eltDom.setAttribute("aria-haspopup","true");
  eltDom.setAttribute("aria-expanded","false");

}

const openDropdown = (eltDom)=> {
  eltDom.setAttribute("aria-haspopup","false");
  eltDom.setAttribute("aria-expanded","true");
}