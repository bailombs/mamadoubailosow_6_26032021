import { displayPhotograhersByTag } from "./photographers.js";

const queryTags = window.location.search;
const urlParams = new URLSearchParams(queryTags);
const eltTags = urlParams.get('tag');

const tags = ["Portrait","Art","Fashion","Architecture","Travel","Sport","Animals","Events"];
let urlTags = [];

if(eltTags !== null ) {
  urlTags = JSON.parse(eltTags)
}

const recoverData = async () => {
  const dataFile =  await fetch("./js/data.json")
  const data = await dataFile.json()
  
 
  const containLink = document.querySelector('.link');

  window.addEventListener('scroll', () =>{
    if(window.scrollY > 30) {
      containLink.style.visibility= "visible";
    } else {
      containLink.style.visibility= "hidden";
    }
  })

  const containerLogo = document.createElement("div");
  const linkNavBar = document.createElement("nav");
  const logoLink = document.createElement('a');
  let header = document.querySelector('.header');
  header.appendChild(containerLogo);
  containerLogo.appendChild(logoLink);
  containerLogo.classList.add("header__logo");
  containerLogo.setAttribute("aria-labelledby", "logo");
  header.appendChild(linkNavBar);

  logoLink.href="index.html";
  logoLink.classList.add("header__logo--link");
  const logo = document.createElement('img');
  logoLink.appendChild(logo);

  logo.src = "css/images/Logo/Logo.png"; 
  logo.alt="logo - page d'accueil"
  logo.id = "logo";

  for(let tag of tags) {

    const link = document.createElement('a');
    linkNavBar.appendChild(link);
    link.classList.add("header__navBar--link")
    const eltTag = document.createElement('span');
    eltTag.setAttribute("role","complementary");
    
    link.appendChild(eltTag);
    link.href ='#';
    eltTag.textContent = "#"+ tag; 
    if(urlTags.includes(tag)) {
      link.classList.add('active');
    }


    link.addEventListener('click', (e) => {
       if(link.hash.slice(1)) {
         link.classList.toggle('active')
       }
      
      if(urlTags.includes(tag)) {        
        urlTags = urlTags.filter((t) => t !== tag); // retirer l'element du tableau
      } else {
        urlTags.push(tag);        
      }
      
      if(link.href === location.href) {
        e.preventDefault();       
        window.location.href = "index.html";
        link.setAttribute("aria-current", "page");          
      } else {        
        e.preventDefault();       
        window.location.href = "index.html?tag="+ JSON.stringify(urlTags); 
      
      
      
        link.setAttribute("aria-current", "page");
      }                   
    })
    const currentLocation = location.href;
    
    if(link.href === currentLocation) {
      
      link.setAttribute("aria-current"," page");  
    } 
  }

 
  linkNavBar.classList.add("header__navBar");
  header.nextElementSibling.classList.add("main");

  //displayPhotographers();
  displayPhotograhersByTag(data, urlTags);
  
}

recoverData();

