export const formModal = (backGround, photographerName, buttonClose) => {
   // DOM
  const content = document.createElement('div');  
  const contactName = document.createElement('h1');
  const  modalBody = document.createElement('div');
  const formModal = document.createElement('form');

  const firstName = document.createElement('div');
  const lastName = document.createElement('div');
  const email = document.createElement('div');    
  const contentText = document.createElement('div');
 // const send = document.createElement('div')
  const submit = document.createElement('input');

  let firstNameLabel = document.createElement("label");
  let firstNameInput = document.createElement("input");
  
  let lastNameLabel = document.createElement("label");
  let lastNameInput = document.createElement("input");

  let emailLabel = document.createElement("label");
  let emailInput = document.createElement("input");

  let textFieldLabel = document.createElement("label");
  let textFieldInput = document.createElement("textarea");


  let message = document.createElement("div");
  // saut de lignes
  const  br1 = document.createElement('br');
  const  br2 = document.createElement('br');
  const  br3 = document.createElement('br');
  const  br4 = document.createElement('br');


  

// Ascendance des elements
  backGround.appendChild(content);   
  content.appendChild(contactName);
  content.appendChild(buttonClose);
  content.appendChild(modalBody);
  modalBody.appendChild(formModal);

  formModal.appendChild(firstName);
  formModal.appendChild(lastName);
  formModal.appendChild(email);
  formModal.appendChild(contentText);
  formModal.appendChild(submit);
  // label et input 
  firstName.appendChild(firstNameLabel);
  firstName.appendChild(br1);
  firstName.appendChild(firstNameInput);
  firstName.appendChild(message);

  lastName.appendChild(lastNameLabel);
  lastName.appendChild(br2);
  lastName.appendChild(lastNameInput);
  lastName.appendChild(message);

  email.appendChild(emailLabel);
  email.appendChild(br3);
  email.appendChild(emailInput);
  email.appendChild(message);

  contentText.appendChild(textFieldLabel);
  contentText.appendChild(br4);
  contentText.appendChild(textFieldInput);
    
 

    
  

// nom des classes et attributs

  content.classList.add('bground__content');

  contactName.classList.add('bground__content--contactName');
  contactName.innerHTML = `Contactez-moi <br><span>${photographerName}</span>`;
  modalBody.classList.add('bground__content--modal-body');
  formModal.id =" contact";
  formModal.setAttribute("action", "index.html");
  formModal.setAttribute("method", "POST");
  firstName.classList.add("formData");
  lastName.classList.add("formData");
  email.classList.add("formData");
  contentText.classList.add("formData");
  firstName.id ="data-first";
  lastName.id ="data-seocnt";
  email.id ="data-third";
  contentText.id ="data-fourth";

  submit.classList.add("btn-submit");
  submit.setAttribute("type", "submit");
  submit.setAttribute("value","Envoyer");
  submit.setAttribute('aria-label', 'envoyer le formulaire');
  
  firstNameLabel.setAttribute("for","first");
  firstNameLabel.innerHTML = "Prénom";
  firstNameInput.classList.add('text-control');
  firstNameInput.setAttribute("type","text");
  firstNameInput.setAttribute("name", "prenom");
  firstNameInput.id = "first";

  lastNameLabel.setAttribute("for","last");
  lastNameLabel.innerHTML = "Nom";
  lastNameInput.classList.add('text-control');
  lastNameInput.setAttribute("type","text");
  lastNameInput.setAttribute("name", "nom");
  lastNameInput.id = "last";

  emailLabel.setAttribute("for","email");
  emailLabel.innerHTML = "Email";
  emailInput.classList.add('text-control');
  emailInput.setAttribute("type","text");
  emailInput.setAttribute("name", "email");
  emailInput.id = "email";

  textFieldLabel.setAttribute("for","textarea");
  textFieldLabel.innerHTML = "Votre message";
  textFieldInput.classList.add('textarea-control');
  textFieldInput.setAttribute("type","textarea");
  textFieldInput.setAttribute("name", "textarea");
  textFieldInput.id ="textarea"
  textFieldInput.placeholder= "votre message";
  textFieldInput.rows = "10" ;

  formModal.addEventListener("submit", (e) =>{
    e.preventDefault()
    const  firstNameValue = firstNameInput.value;
    const  lastNameValue = lastNameInput.value;
    const  emailValue = emailInput.value;
    const  textFieldValue = textFieldInput.value;
            
    console.log("prenom :", firstNameValue);
    console.log("Nom :", lastNameValue);  
    console.log("Email :", emailValue);
    console.log("Votre texte :", textFieldValue);
  })
  
}
 
 // accessibility  le formulaire 
const mainWrapper= document.querySelector('#main-wrapper');
 
 const modalformCloseButton = document.querySelector('.bground__content--close');

export const onOpenModalForm = (modalform) => {
  mainWrapper.setAttribute('aria-hidden','true');
  modalform.setAttribute('aria-hidden', 'false');
  modalform.style.display = "block";
  modalformCloseButton.focus();

}

export const onCloseModalForm = (modalform, modalOpenButton) => {
  mainWrapper.setAttribute('aria-hidden','false');
  modalform.setAttribute('aria-hidden', 'true'); 
  modalform.style.display ="none";
  modalOpenButton.focus();      
}




